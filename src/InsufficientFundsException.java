/**
 * décembre 2015
 * Indicates that an account has insufficient funds to complete an operation.
 * @author Mirko Steinle
 */
public class InsufficientFundsException extends RuntimeException
{
    public InsufficientFundsException(String operation, Account account)
    {
        super("Insufficient funds for [" + operation + "] on account: " + account);
    }
}
