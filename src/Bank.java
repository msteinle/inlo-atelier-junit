import java.util.ArrayList;
import java.util.List;

/**
 * décembre 2015
 *
 * @author Mirko Steinle
 */
public class Bank
{
    private List<Account> accounts;

    public Bank(List<Account> accounts)
    {
        this.accounts = new ArrayList<>(accounts);
    }

    public Account findAccount(String accountNumber)
    {
        for (Account a : accounts)
        {
            if (a.getAccountNumber().equals(accountNumber))
            {
                return a;
            }
        }
        return null;
    }

    public Account findBiggestAccount()
    {
        Account result = null;
        for (Account a : accounts)
        {
            if (result == null)
            {
                result = a;
            }
            else if (result.getBalance() < a.getBalance())
            {
                result = a;
            }
        }
        return result;
    }


    public List<Account> findBigAccounts(double minBalance)
    {
        List<Account> result = new ArrayList<>();
        for (Account a : accounts)
        {
            if (a.getBalance() > minBalance)
            {
                result.add(a);
            }
        }
        return result;
    }
}
