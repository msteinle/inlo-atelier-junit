import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * décembre 2015
 *
 * @author Mirko Steinle
 */
public class BankTest
{

    @Test
    public void testFindAccount_noMatch() throws Exception
    {
        // setup
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(new Account("a1", 30, 0));
        accounts.add(new Account("a2", 100, 0));
        accounts.add(new Account("a3", 100, 0));
        Bank bankWithAccounts = new Bank(accounts);
        // exercice
        Account result = bankWithAccounts.findAccount("a4");
        // verify
        Assert.assertNull(result);
    }

    @Test
    public void testFindAccount_match() throws Exception
    {
        // setup
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(new Account("a1", 30, 0));
        accounts.add(new Account("a2", 100, 0));
        accounts.add(new Account("a3", 100, 0));
        Bank bankWithAccounts = new Bank(accounts);
        // exercice
        Account result = bankWithAccounts.findAccount("a2");
        // verify
        Assert.assertNotNull(result);
        Assert.assertEquals("a2", result.getAccountNumber());
    }
}