import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * décembre 2015
 *
 * @author Mirko Steinle
 */
public class AccountTest4
{

    Account a;

    @Before
    public void setUp() throws Exception
    {
        a = new Account("a", 30.5, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCredit_negativeAmount() throws Exception
    {
        a.credit(-10);
    }

    @Test
    public void testCredit_negativeAmount2() throws Exception
    {
        try
        {
            a.credit(-10);
            Assert.fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException e)
        {
            Assert.assertEquals("Le solde ne devrait pas changer", 30.5, a.getBalance(), 0);
        }
    }

}
